import { User } from "../models/user";


export const Actions = Object.freeze({
    setUser: (user?: User) => ({ type: 'set', payload: user })
});

interface State {
    user: User | null;
    list: User[];
}

const initialState: State = {
    user: null,
    list:[
        {username:"username1",password:"userpassword1"},
        {username:"username2",password:"userpassword2"},
    ],
}

const userReducer = (state = initialState, action: any) => {
    console.log('user',state);
    switch (action.type) {
        case'set':
            const localuser = action.payload;
            if(localuser){
                const localList = [...state.list];
                const index = localList.findIndex(x => x.username === localuser.username);
                if(index === -1){
                    localList.push(localuser);
                }
                const newState = { ...state, user: localuser, list: [...localList] };
                return newState;
            }
            else{
                return { ...state, user: localuser };
            }
        default:
            return state;
    }
};
export default userReducer;