import React from 'react';
import { User } from "./models/user";
import store from '../store';
import { Actions } from "./StateManagement/userReducer";

const withState = (EnterComponent:any) => {
  class StateHoc extends React.Component {

    render() {
      const user = store.getState().user.user;
      const userListFromGs = store.getState().user.list;

      const exit = () => {
        store.dispatch(Actions.setUser(undefined));
      };

      const select = (user:User) => {
        store.dispatch(Actions.setUser(user));
    };

      return (
        <div>
            <EnterComponent userLocal={user} onExit={exit} onSelect={select} userList={userListFromGs}/>
        </div>
      );
    }
  }
    
  return StateHoc;
};

export default withState;
