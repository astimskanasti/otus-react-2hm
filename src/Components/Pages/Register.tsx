import { Box, Button, Container, CssBaseline, TextField, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { User } from "../models/user";
import StateHoc from "../StateHoc";

interface Props {
    userLocal: User,
    onExit: () => void,
    onSelect: (user: User) => void,
    userList: User[]
}

const regFunc = function Register(props: Props) {
    let userName = '';
    let userPassword = '';
    let userEmail = '';
    const navigate = useNavigate();

    const onLocalExit = () => {
        props.onExit();
        navigate('/homepage');
    }

    const handleSubmit = () => {
        const localUser = { username: userName, password: userPassword, email: userEmail }
        props.onSelect(localUser);
        navigate('/homepage');
    };

    const setUserName = (e: any) => {
        userName = e.target.value;
    }
    const setUserPassword = (e: any) => {
        userPassword = e.target.value;
    }
    const setEmail = (e: any) => {
        userEmail = e.target.value;
    }


    return <div>
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Let's sign in
                </Typography>
                <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        onChange={setUserName}
                        name="username"
                        autoComplete="username"
                        autoFocus
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        onChange={setUserPassword}
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Sign Up
                    </Button>
                    <Button
                        onClick={onLocalExit}
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        На главную
                    </Button>
                </Box>
            </Box>
        </Container>
    </div>
}

const RegWithCommon = StateHoc(regFunc);

export default RegWithCommon;