import { User } from "../models/user";
import Button from '@mui/material/Button';
import { Box, Container, CssBaseline, TextField, Typography, } from "@mui/material";
import { useNavigate } from "react-router-dom";
import StateHoc from "../StateHoc";


interface Props {
    userLocal: User,
    onExit: () => void,
    onSelect: (user: User) => void,
    userList: User[]
}

const loginFunc = function Login(props: Props) {
    let userName = '';
    let userPassword = '';

    const navigate = useNavigate();

    const onLocalExit = () => {
        props.onExit();
        navigate('/homepage');
    }

    const handleSubmit = () => {
        const matchedUser = props.userList.find((x: User) => x.username === userName && x.password === userPassword);
        if (matchedUser) {
            const localUser = matchedUser;
            props.onSelect(localUser);
            navigate('/homepage');
        }
        else {
            alert('Invalid username or password');
            navigate('/register');
        }
    };

    const setUserName = (e: any) => {
        userName = e.target.value;
    }

    const setPassword = (e: any) => {
        userPassword = e.target.value;
    }


    return <div>
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                <Typography component="h1" variant="h5">
                    Let's sign in
                </Typography>
                <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        onChange={setUserName}
                        autoComplete="username"
                        autoFocus
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        onChange={setPassword}
                        id="password"
                        autoComplete="current-password"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Sign In
                    </Button>
                    <Button
                        onClick={onLocalExit}
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                    >
                        На главную
                    </Button>
                </Box>
            </Box>
        </Container>
    </div>
}

const LoginWithCommon = StateHoc(loginFunc);

export default LoginWithCommon;


