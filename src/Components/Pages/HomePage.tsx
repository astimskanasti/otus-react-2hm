import { Box, Button, Container, Typography } from "@mui/material";
import { connect, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const HomePage = () => {
    const navigate = useNavigate();
    const userFromGs = useSelector((gs: any) => gs.user.user);
    const msgName = userFromGs ? 'Привет, ' + userFromGs.username : 'Необходимо войти или зарегистрироваться';

    const onLogin = () => {
        navigate('/login');
    }

    return (
        <div>
            <Container component="main" maxWidth="xs">
                <Box sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    <Typography
                        component="h1"
                        variant="h2"
                        align="center"
                        color="text.primary"
                        gutterBottom
                    >
                        Главная страница
                    </Typography>
                    <Typography
                        component="h1"
                        align="center"
                        color="text.primary"
                        gutterBottom
                    > {msgName}
                    </Typography>
                    <Box sx={{ flexDirection: 'row', display: 'flex', justifyContent: 'center' }}>
                        <Button variant="contained" href="/login" sx={{ width: 1 / 4, mr: 2 }} >Вход</Button>
                        <Button variant="contained" href="/register" sx={{ width: 1 / 2, mr: 2 }}  >Регистрация</Button>
                        <Button variant="contained" sx={{ width: 1 / 2, mr: 2 }} onClick={onLogin}>Выйти</Button>
                    </Box>

                </Box>
            </Container>
        </div>
    );
}

export default HomePage;