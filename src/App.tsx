

import './App.css';
import {
  Route,
  Routes,
  BrowserRouter
} from "react-router-dom";
import HomePage  from './Components/Pages/HomePage';
import Register from './Components/Pages/Register';
import Login from './Components/Pages/Login';
import ErrorPage from './Components/Pages/ErrorPage';



function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login/>} />
          <Route path="/register" element={<Register/>} />
          <Route path="/homepage" element={<HomePage/>}/>
          <Route path="/" element={<HomePage/>}/>
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
