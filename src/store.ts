import { configureStore } from '@reduxjs/toolkit';
import userReducer from './Components/StateManagement/userReducer';

const store = configureStore({
    reducer: {
        user: userReducer
    },

});
export default store;